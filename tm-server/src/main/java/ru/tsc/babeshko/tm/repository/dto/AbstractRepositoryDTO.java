package ru.tsc.babeshko.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.babeshko.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.babeshko.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Getter
@Repository
@Scope("prototype")
public abstract class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    protected final Class<M> clazz;

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    public AbstractRepositoryDTO(@NotNull final Class<M> clazz) {
        this.clazz = clazz;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void set(@NotNull final Collection<M> models) {
        clear();
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(entityManager.getReference(clazz, model.getId()));
    }

}