package ru.tsc.babeshko.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.babeshko.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.babeshko.tm.api.service.dto.IUserOwnedServiceDTO;
import ru.tsc.babeshko.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.tsc.babeshko.tm.enumerated.Sort;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.exception.field.EmptyDescriptionException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyNameException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.exception.system.EmptyStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.Comparator;
import java.util.List;

@Service
public abstract class AbstractUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedRepositoryDTO<M>>
        extends AbstractServiceDTO<M, R>
        implements IUserOwnedServiceDTO<M> {

    @NotNull
    @Override
    protected abstract IUserOwnedRepositoryDTO<M> getRepository();

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        repository.clear(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return findAll(userId);
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.findAll(userId, sort.getComparator());
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.existsById(userId, id);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.findOneById(userId, id);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        repository.remove(userId, model);
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        repository.removeById(userId, id);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        repository.update(model);
    }

    @Nullable
    @Override
    @Transactional
    public M updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        @Nullable final M model = repository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.update(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public M changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        @Nullable final M model = repository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.update(model);
        return model;
    }

    @Override
    public long getCount(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.getCount(userId);
    }

}