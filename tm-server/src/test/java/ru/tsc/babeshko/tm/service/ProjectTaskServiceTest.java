package ru.tsc.babeshko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.babeshko.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.babeshko.tm.configuration.ContextConfiguration;
import ru.tsc.babeshko.tm.dto.model.ProjectDTO;
import ru.tsc.babeshko.tm.dto.model.TaskDTO;
import ru.tsc.babeshko.tm.dto.model.UserDTO;
import ru.tsc.babeshko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;

public class ProjectTaskServiceTest {

    @NotNull
    private IProjectTaskServiceDTO projectTaskService;

    @NotNull
    private IUserServiceDTO userService;

    @NotNull
    private IProjectServiceDTO projectService;

    @NotNull
    private ITaskServiceDTO taskService;

    private String userId;

    private String projectId;

    private String taskId;

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ContextConfiguration.class);
        projectTaskService = context.getBean(IProjectTaskServiceDTO.class);
        projectService = context.getBean(IProjectServiceDTO.class);
        userService = context.getBean(IUserServiceDTO.class);
        taskService = context.getBean(ITaskServiceDTO.class);
        @NotNull final UserDTO user = userService.create("user", "user");
        userId = user.getId();
        @NotNull final ProjectDTO project = projectService.create(userId, "project");
        projectId = project.getId();
        @NotNull final TaskDTO task = taskService.create(userId, "task");
        taskId = task.getId();
    }

    @After
    public void end() {
        taskService.clear(userId);
        projectService.clear(userId);
        userService.removeByLogin("user");
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.bindTaskToProject("", projectId, taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(userId, "", taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, "not_task_id"));

        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        @NotNull final TaskDTO task = taskService.findOneById(taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.unbindTaskFromProject("", projectId, taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, "", taskId));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, projectId, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, projectId, "not_task_id"));
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        @NotNull final TaskDTO task = taskService.findOneById(taskId);
        Assert.assertNull(task.getProjectId());
    }

}